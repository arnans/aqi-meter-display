# AQI Meter Display

An AQI meter display using a 24 Neo Pixel module.  
- Uses the ESP32 Wemos Lolin32 compatible board, with a built-in OLED display
- Drives the Neo Pixels using hardware RMT on the ESP32 
- Gets the PM2.5 value from a remote node over MQTT
